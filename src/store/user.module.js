import UserService from '../services/user.service';

const initialState = null;

export const user = {
    namespaced: true,
    state: initialState,
    actions: {

        getHello({ commit }) {
            return UserService.getContent().then(
                response => {
                    commit('getSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('loginFailure');
                    return Promise.reject(error);
                }
            );
        }
    },

    mutations: {
        getSuccess(state, response) {
            state.response = response;
        }
    }
};