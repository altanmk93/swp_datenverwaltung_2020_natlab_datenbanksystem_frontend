import StudentService from '../services/student.service';

const initialState = null;

export const student = {

    namespaced: true,
    state: initialState,
    actions: {

        insert({ commit }, student) {
            return StudentService.insert(student).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        update({ commit }, student) {
            return StudentService.update(student).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        delete({ commit }, id) {
            return StudentService.delete(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        getAll({ commit }) {
            return StudentService.getAll().then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        getByID({ commit }, id) {
            return StudentService.getByID(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        exportExcel({ commit },ids) {
            return StudentService.exportExcel(ids).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        }
    },

    mutations: {
        insertSuccess(state, response) {
            state.response = response;
        },
        insertFailure(state, response) {
            state.response = response;
        }
    }
};