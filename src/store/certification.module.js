import CertificationService from '../services/certification.service';

const initialState = null;

export const certification = {

    namespaced: true,
    state: initialState,
    actions: {

        insert({ commit }, certification) {
            return CertificationService.insert(certification).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        update({ commit }, certification) {
            return CertificationService.update(certification).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        delete({ commit }, id) {
            return CertificationService.delete(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        getAll({ commit }) {
            return CertificationService.getAll().then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        getByID({ commit }, id) {
            return CertificationService.getByID(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        exportExcel({ commit }, ids) {
            return CertificationService.exportExcel(ids).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        }
    },

    mutations: {
        insertSuccess(state, response) {
            state.response = response;
        },
        insertFailure(state, response) {
            state.response = response;
        }
    }
};