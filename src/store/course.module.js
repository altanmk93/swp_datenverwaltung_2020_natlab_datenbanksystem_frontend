import CourseService from '../services/course.service';

const initialState = null;

export const course = {

    namespaced: true,
    state: initialState,
    actions: {

        insert({ commit }, course) {
            return CourseService.insert(course).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        update({ commit }, course) {
            return CourseService.update(course).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        delete({ commit }, id) {
            return CourseService.delete(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        getAll({ commit }) {
            return CourseService.getAll().then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        getByID({ commit }, id) {
            return CourseService.getByID(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        exportExcel({ commit },ids) {
            return CourseService.exportExcel(ids).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        }
    },

    mutations: {
        insertSuccess(state, response) {
            state.response = response;
        },
        insertFailure(state, response) {
            state.response = response;
        }
    }
};