import Vue from 'vue';
import Vuex from 'vuex';

import { auth } from './auth.module';
import { user } from './user.module';
import { course } from './course.module';
import { student } from './student.module';
import { teacher } from './teacher.module';
import { event } from './event.module';
import { certification } from './certification.module';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        user,
        course,
        student,
        event,
        teacher,
        certification
    }
});