import TeacherService from '../services/teacher.service';

const initialState = null;

export const teacher = {

    namespaced: true,
    state: initialState,
    actions: {

        insert({ commit }, teacher) {
            return TeacherService.insert(teacher).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        update({ commit }, teacher) {
            return TeacherService.update(teacher).then(
                response => {
                    commit('updateSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('updateFailure');
                    return Promise.reject(error);
                }
            );
        },

        getAll({ commit }) {
            return TeacherService.getAll().then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        getByID({ commit }, id) {
            return TeacherService.getByID(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        delete({ commit }, id) {
            return TeacherService.delete(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        exportExcel({ commit }, ids) {
            return TeacherService.exportExcel(ids).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        }
    },

    mutations: {
        insertSuccess(state, response) {
            state.response = response;
        },
        insertFailure(state, response) {
            state.response = response;
        }
    }
};