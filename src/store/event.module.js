import EventService from '../services/event.service';

const initialState = null;

export const event = {

    namespaced: true,
    state: initialState,
    actions: {

        insert({ commit }, event) {
            return EventService.insert(event).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

        get({ commit }) {
            return EventService.get().then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },


        delete({ commit }, id) {
            return EventService.delete(id).then(
                response => {
                    commit('insertSuccess', response);
                    return Promise.resolve(response);
                },
                error => {
                    commit('insertFailure');
                    return Promise.reject(error);
                }
            );
        },

    },

    mutations: {
        insertSuccess(state, response) {
            state.response = response;
        },
        insertFailure(state, response) {
            state.response = response;
        }
    }
};