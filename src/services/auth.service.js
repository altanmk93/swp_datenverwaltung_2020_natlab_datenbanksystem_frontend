import axios from 'axios';
import authHeader from "@/services/auth-header";

const API_URL = 'http://localhost:9192/';

class AuthService {

    login(user) {
        return axios
            .post(API_URL + 'authenticate', {
                username: user.username,
                password: user.password
            })
            .then(response => {
                if (response.data.idToken) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                    localStorage.setItem('cronJob', 0);
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'register', {
            firstname: user.firstname,
            name: user.name,
            username: user.username,
            email: user.email,
            password: user.password
        },{ headers: authHeader() });
    }

    isExpired(user) {
        return axios.post(API_URL + 'isExpired', {
                expiresIn: user.expiresIn
        });
    }
}

export default new AuthService();
