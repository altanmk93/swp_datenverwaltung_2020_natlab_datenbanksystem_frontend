import axios from 'axios';
import authHeader from "@/services/auth-header";

const API_URL = 'http://localhost:9192/';

class EventService {

    insert(event) {
        return axios
            .post(API_URL + 'insertEvent', {
                    title: event.title,
                    description: event.description,
                    startTime: event.startTime,
                    endTime: event.endTime
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    get() {
        return axios
            .get(API_URL + 'getEvents', {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    delete(id) {
        return axios
            .get(API_URL + 'deleteEvent/'+id, {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

}


export default new EventService();