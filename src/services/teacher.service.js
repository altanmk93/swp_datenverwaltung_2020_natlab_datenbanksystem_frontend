import axios from 'axios';
import authHeader from "@/services/auth-header";
import fileDownload from "js-file-download";

const API_URL = 'http://localhost:9192/';

class TeacherService {

    insert(teacher) {
        return axios
            .post(API_URL + 'insertTeacher', {
                    personal_number: teacher.personal_number,
                    gender: teacher.gender,
                    grad: teacher.grad,
                    firstname: teacher.firstname,
                    lastname: teacher.lastname,
                    email: teacher.email,
                    telephone: teacher.telephone,
                    street: teacher.street,
                    zip: teacher.zip,
                    city: teacher.city,
                    subject: teacher.subject,
                    school_branch: teacher.school_branch,
                    school_name: teacher.school_name,
                    school_number: teacher.school_number,
                    status: teacher.status,
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    update(teacher) {
        return axios
            .post(API_URL + 'updateTeacher', {
                    id:teacher.id,
                    personal_number: teacher.personal_number,
                    gender: teacher.gender,
                    grad: teacher.grad,
                    firstname: teacher.firstname,
                    lastname: teacher.lastname,
                    email: teacher.email,
                    telephone: teacher.telephone,
                    street: teacher.street,
                    zip: teacher.zip,
                    city: teacher.city,
                    subject: teacher.subject,
                    school_branch: teacher.school_branch,
                    school_name: teacher.school_name,
                    school_number: teacher.school_number,
                    status: teacher.status,
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    delete(id) {
        return axios
            .post(API_URL + 'deleteTeacher', {
                    id: id
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    getAll() {
        return axios
            .get(API_URL + 'getTeachers', {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    getByID(idValue) {
        return axios
            .get(API_URL + 'getTeacherByID/'+idValue, {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }


    exportExcel(ids) {
        let idsArray = ""
        if (ids.length > 0)
            idsArray =  "/";

        for (let i = 0; i < ids.length; i++) {
            if (i === ids.length-1)
                idsArray += ids[i];
            else
                idsArray += ids[i]+",";
        }

        return axios
            .get(API_URL + 'exportTeacherExcel' + idsArray, {
                headers: authHeader(),
                responseType: 'blob',
            })
            .then(response => {
                fileDownload(response.data, "Lehrkräfte.xlsx");
                return response.data;
            });
    }
}


export default new TeacherService();