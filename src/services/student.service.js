import axios from 'axios';
import authHeader from "@/services/auth-header";
import fileDownload from 'js-file-download';

const API_URL = 'http://localhost:9192/';

class StudentService {

    insert(student) {
        return axios
            .post(API_URL + 'insertStudent', {
                    anrede: student.anrede,
                    firstname: student.firstname,
                    lastname: student.lastname,
                    street: student.street,
                    postcode: student.zip,
                    ort: student.city,
                    sommeruni_17: student.sommeruni17,
                    sommeruni_18: student.sommeruni18,
                    sommeruni_19: student.sommeruni19,
                    sommeruni_20: student.sommeruni20,
                    natuerlich_2020: student.natuerlich20,
                    status: student.status
                }

                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    update(student) {
        return axios
            .post(API_URL + 'updateStudent', {
                    id: student.id,
                    anrede: student.anrede,
                    firstname: student.firstname,
                    lastname: student.lastname,
                    street: student.street,
                    postcode: student.zip,
                    ort: student.city,
                    sommeruni_17: student.sommeruni17,
                    sommeruni_18: student.sommeruni18,
                    sommeruni_19: student.sommeruni19,
                    sommeruni_20: student.sommeruni20,
                    natuerlich_2020: student.natuerlich20,
                    status: student.status
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    delete(id) {
        return axios
            .post(API_URL + 'deleteStudent', {
                    id: id
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    getAll() {
        return axios
            .get(API_URL + 'getStudents', {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    getByID(idValue) {
        return axios
            .get(API_URL + 'getStudentByID/'+idValue, {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    exportExcel(ids) {
        let idsArray = ""
        if (ids.length > 0)
            idsArray =  "/";

        for (let i = 0; i < ids.length; i++) {
            if (i === ids.length-1)
                idsArray += ids[i];
            else
                idsArray += ids[i]+",";
        }

        return axios
            .get(API_URL + 'exportStudentExcel' + idsArray, {
                headers: authHeader(),
                responseType: 'blob',
            })
            .then(response => {
                fileDownload(response.data, "Student.xlsx");
                return response.data;
            });
    }

}


export default new StudentService();