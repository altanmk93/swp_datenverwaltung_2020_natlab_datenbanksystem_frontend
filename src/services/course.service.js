import axios from 'axios';
import authHeader from "@/services/auth-header";
import fileDownload from 'js-file-download';

const API_URL = 'http://localhost:9192/';

class CourseService {

    insert(course) {
        return axios
            .post(API_URL + 'insertCourse', {
                    course_number: course.coursenumber,
                    course_offer: course.courseoffer,
                    course_name: course.coursename,
                    event_type: course.eventtype,
                    course_datetime: course.coursedate,
                    course_topic: course.topic,
                    course_location: course.courselocation,
                    accompanyingTeacher: course.accompanyingteacher,
                    supervisingStudents: course.supervisingstudents,
                    presentStudents:course.presentstudents,
                    registeredStudents:course.registeredstudents,
                    anzahlung: course.anzahlung,
                    honorar: course.honorar,
                    instructor: course.instructor,
                    noteField: course.notefield,
                    status: course.status
                }

                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    update(course) {
        return axios
            .post(API_URL + 'updateCourse', {
                    id: course.id,
                    course_number: course.coursenumber,
                    course_offer: course.courseoffer,
                    course_name: course.coursename,
                    event_type: course.eventtype,
                    course_datetime: course.coursedate,
                    course_topic: course.topic,
                    course_location: course.courselocation,
                    accompanyingTeacher: course.accompanyingteacher,
                    supervisingStudents: course.supervisingstudents,
                    presentStudents:course.presentstudents,
                    registeredStudents:course.registeredstudents,
                    anzahlung: course.anzahlung,
                    honorar: course.honorar,
                    instructor: course.instructor,
                    noteField: course.notefield,
                    status: course.status
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    delete(id) {
        return axios
            .post(API_URL + 'deleteCourse', {
                    id: id
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    getAll() {
        return axios
            .get(API_URL + 'getCourses', {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    getByID(idValue) {
        return axios
            .get(API_URL + 'getCoursesByID/'+idValue, {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    exportExcel(ids) {
        let idsArray = ""
        if (ids.length > 0)
            idsArray =  "/";

        for (let i = 0; i < ids.length; i++) {
            if (i === ids.length-1)
                idsArray += ids[i];
            else
                idsArray += ids[i]+",";
        }

        return axios
            .get(API_URL + 'exportCourseExcel' + idsArray, {
                headers: authHeader(),
                responseType: 'blob',
            })
            .then(response => {
                fileDownload(response.data, "Kurse.xlsx");
                return response.data;
            });
    }

}


export default new CourseService();