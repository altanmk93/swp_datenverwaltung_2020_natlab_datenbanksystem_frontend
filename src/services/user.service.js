import axios from 'axios';
import authHeader from "@/services/auth-header";

const API_URL = 'http://localhost:9192/';

class UserService {

    getContent() {
        return axios
            .get(API_URL + 'hello', {headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }
}


export default new UserService();