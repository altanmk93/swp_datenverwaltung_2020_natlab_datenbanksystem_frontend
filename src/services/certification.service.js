import axios from 'axios';
import authHeader from "@/services/auth-header";
import fileDownload from "js-file-download";

const API_URL = 'http://localhost:9192/';

class CertificationService {

    insert(certification) {
        return axios
            .post(API_URL + 'insertCertification', {
                    certifications_id: certification.certificationid,
                    certificationtype: certification.certificationtype,
                    certificationname: certification.certificationname,
                    certificationowner: certification.certificationowner,
                    certificationdate: certification.certificationdate,
                    certificationexpiredate: certification.certificationexpiredate,
                    status: certification.status
                }

                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    update(certification) {
        return axios
            .post(API_URL + 'updateCertification', {
                    id: certification.id,
                    certifications_id: certification.certificationid,
                    certificationtype: certification.certificationtype,
                    certificationname: certification.certificationname,
                    certificationowner: certification.certificationowner,
                    certificationdate: certification.certificationdate,
                    certificationexpiredate: certification.certificationexpiredate,
                    status: certification.status
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    delete(id) {
        return axios
            .post(API_URL + 'deleteCertification', {
                    id: id
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    getAll() {
        return axios
            .get(API_URL + 'getCertifications', {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    getByID(idValue) {
        return axios
            .get(API_URL + 'getCertificationByID/'+idValue, {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    exportExcel(ids) {
        let idsArray = ""
        if (ids.length > 0)
            idsArray =  "/";

        for (let i = 0; i < ids.length; i++) {
            if (i === ids.length-1)
                idsArray += ids[i];
            else
                idsArray += ids[i]+",";
        }

        return axios
            .get(API_URL + 'exportCertificationExcel' + idsArray, {
                headers: authHeader(),
                responseType: 'blob',
            })
            .then(response => {
                fileDownload(response.data, "Fortbildungen.xlsx");
                return response.data;
            });
    }
}


export default new CertificationService();