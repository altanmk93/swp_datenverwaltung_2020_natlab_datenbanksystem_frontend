import Vue      from 'vue'
import Router   from 'vue-router'

import Login    from "./components/LogIn";
import Register from "./components/SignIn";

import Home     from "./components/Home";
import Courses  from "./components/Courses";
import Calendar from "./components/Calendar";
import Teachers from "./components/Teachers";
import CreateCourse from "@/components/CreateEditComponents/CreateCourse";
import EditCourse from "@/components/CreateEditComponents/EditCourse";
import CreateTeacher from "@/components/CreateEditComponents/CreateTeacher";
import EditTeacher from "@/components/CreateEditComponents/EditTeacher";
import Certifications from "@/components/Certifications";
import CreateCertification from "@/components/CreateEditComponents/CreateCertification";
import EditCertificate from "@/components/CreateEditComponents/EditCertificate";
import Student from "@/components/Student";
import CreateStudent from "@/components/CreateEditComponents/CreateStudent";
import EditStudent from "@/components/CreateEditComponents/EditStudent";

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base:  process.env.BASE_URL,
    routes: [
        // Button from Courses
        {
            path: '/createcourse',
            name: 'neuen Eintrag hinzufügen',
            component: CreateCourse
        },
        {
            path: '/editcourse',
            name: 'Eintrag bearbeiten',
            component: EditCourse
        },

        // Button from Students
        {
            path: '/createstudent',
            name: 'neuen Eintrag hinzufügen',
            component: CreateStudent
        },
        {
            path: '/editstudent',
            name: 'Eintrag bearbeiten',
            component: EditStudent
        },

        // Button from Teachers
        {
            path: '/createteacher',
            name: 'neuen Eintrag hinzufügen',
            component: CreateTeacher
        },
        {
            path: '/editteacher',
            name: 'Eintrag bearbeiten',
            component: EditTeacher
        },

        // Button from Certification
        {
            path: '/createcertification',
            name: 'neuen Eintrag hinzufügen',
            component: CreateCertification
        },
        {
            path: '/editcertification',
            name: 'Eintrag bearbeiten',
            component: EditCertificate
        },
        {
            path: '/deletecertification',
            name: 'Eintrag löschen'
        },

        // LOGIN / REGISTER
        {
            path: '/login',
            name: 'Login von Natlab',
            component: Login
        },
        {
            path: '/register',
            name: 'Register von Natlab',
            component: Register
        },

        // MAIN CONTENT
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/home',
            component: Home
        },
        {
            path: '/students',
            name: 'students',
            component: Student,
        },
        {
            path: '/courses',
            name: 'courses',
            component: Courses,
        },
        {
            path: '/calendar',
            name: 'calendar',
            component: Calendar,
        },
        {
            path: '/teachers',
            name: 'teachers',
            component: Teachers,
        },
        {
            path: '/certifications',
            name: 'certifications',
            component: Certifications,
        }
    ]
})

router.beforeEach((to, from, next) => {
    const publicPages = ['/login'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    // trying to access a restricted page + not logged in
    // redirect to login page
    if (authRequired && !loggedIn) {
        next('/login');
    } else {
        next();
    }
});

export default router