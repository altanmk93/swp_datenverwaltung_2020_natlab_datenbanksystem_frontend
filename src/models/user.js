export default class User {

    constructor(firstname, name, username, email, password) {
        this.firstname = firstname;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }

}