export default class Course {

    constructor(id, coursenumber, courseoffer, coursename, eventtype, coursedate, topic, courselocation, accompanyingteacher, supervisingstudents, presentstudents, registeredstudents, anzahlung, honorar, instructor, notefield, status) {
        this.id = id;
        this.coursenumber = coursenumber;
        this.courseoffer = courseoffer;
        this.coursename = coursename;
        this.eventtype = eventtype;
        this.coursedate = coursedate;
        this.topic = topic;
        this.courselocation = courselocation;
        this.accompanyingteacher = accompanyingteacher;
        this.supervisingstudents = supervisingstudents;
        this.presentstudents = presentstudents;
        this.registeredstudents = registeredstudents;
        this.anzahlung = anzahlung;
        this.honorar = honorar;
        this.instructor = instructor;
        this.notefield = notefield;
        this.status = status;
    }
}
