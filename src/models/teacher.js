export default class Teacher {

    constructor(personal_number, gender, grad, firstname, lastname, email, telephone, street, zip, city, subject, school_branch, school_name, school_number, status) {
        this.personal_number = personal_number;
        this.gender = gender;
        this.grad = grad;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.telephone = telephone;
        this.street = street;
        this.zip = zip;
        this.city = city;
        this.subject = subject;
        this.school_branch = school_branch;
        this.school_name = school_name;
        this.school_number = school_number;
        this.status = status;
    }

}