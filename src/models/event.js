export default class Event {

    constructor(id, title, description, startTime, endTime, status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.status = status;
    }

}