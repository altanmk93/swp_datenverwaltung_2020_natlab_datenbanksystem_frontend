import Vue from 'vue'
import App from './App.vue'

import router from './router'
import store from './store/index'

import BootstrapVue from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import VeeValidate from 'vee-validate'
import VueCrontab  from 'vue-crontab'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faHome, faUser, faUserPlus, faSignInAlt, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'

import VueGoodTablePlugin from 'vue-good-table';

// import the styles
import 'vue-good-table/dist/vue-good-table.css'

Vue.use(VueGoodTablePlugin);

library.add(faHome, faUser, faUserPlus, faSignInAlt, faSignOutAlt);

VueCrontab.setOption({
  interval: 200,
  auto_start: false
})

Vue.config.productionTip = false;
Vue.use(VueCrontab)
Vue.use(BootstrapVue)
Vue.use(VeeValidate)
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  router,
  store,

  render: h => h(App)
}).$mount('#app');
